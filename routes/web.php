<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'WelcomeController@index');

//Route::get('home', 'HomeController@index');
//
//Route::controllers([
//	'auth' => 'Auth\AuthController',
//	'password' => 'Auth\PasswordController',
//]);
Route::model('tasks', 'Task');
Route::model('projects', 'Project');

Route::resource('projects', 'ProjectsController');
Route::resource('projects.tasks', 'TasksController');

Route::bind('tasks', function($value, $route) {
    return App\Task::whereTitle($value)->first();
});
Route::bind('projects', function($value, $route) {
    return App\Project::whereTitle($value)->first();
});

//the above will override the default behavior for the tasks and projects wildcards in php artisan routes.
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
