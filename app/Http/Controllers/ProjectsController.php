<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index(){
        $projects = Project::all();
        return view('projects.index', compact('projects'));
    }

    public function create(){
        return view('projects.create');
    }

    public function show(Project $project){
        return view('projects.show', compact('project'));
    }

    public function edit(Project $project){
        return view('projects.show', compact('project'));
    }

    public function destroy(Project $project)
    {
        //
    }
}
